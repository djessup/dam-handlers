package com.adobe.ams.utils;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.day.cq.dam.api.handler.AssetHandler;
import com.day.cq.dam.api.metadata.ExtractedMetadata;
import com.day.cq.dam.commons.handler.AbstractAssetHandler;
import com.google.common.base.Stopwatch;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.ServiceScope;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.osgi.service.metatype.annotations.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.concurrent.TimeUnit;

/**
 * An {@link AssetHandler} that supports less-common PowerPoint formats.
 *
 * @author jessup
 */
@SuppressWarnings("PackageAccessibility")
@Component(service = AssetHandler.class, scope = ServiceScope.SINGLETON)
@Designate(ocd = PowerPointAssetHandler.Config.class)
public class PowerPointAssetHandler extends AbstractAssetHandler {

    private static final Logger LOG = LoggerFactory.getLogger(PowerPointAssetHandler.class);

    private static final String MIMETYPE_PPTM = "application/vnd.ms-powerpoint.presentation.macroenabled.12";
    private static final String MIMETYPE_PPSX = "application/vnd.openxmlformats-officedocument.presentationml.slideshow";
    private static final String MIMETYPE_POTX = "application/vnd.openxmlformats-officedocument.presentationml.template";
    private static final String MIMETYPE_POTM = "application/vnd.ms-powerpoint.template.macroenabled.12";

    private static final boolean DEFAULT_ANTI_ALIAS = true;
    private static final boolean DEFAULT_SUBPIXEL_RENDERING = true;
    private static final boolean DEFAULT_RENDER_HIGH_QUALTIY = false;

    private String[] supportedMimeTypes;
    private Object antiAliasType;
    private Object subPixelRendering;
    private Object renderQuality;
    private Object renderInterpolation;

    @ObjectClassDefinition(name = "PowerPoint Asset Handler",
            description = "Extracts images from PowerPoint documents")
    public @interface Config {

        @AttributeDefinition(name = "Supported MIME types", description = "List of MIME types that should be parsed by this handler")
        String[] supportedMimeTypes() default {MIMETYPE_PPTM, MIMETYPE_PPSX, MIMETYPE_POTX, MIMETYPE_POTM};

        @AttributeDefinition(name = "Apply anti-aliasing", description = "If enabled antialiasing will be applied when rendering the asset image",
                options = {
                        @Option(label = "Enabled", value = "true"),
                        @Option(label = "Disabled", value = "false")
                }
        )
        boolean antiAlias() default DEFAULT_ANTI_ALIAS;

        @AttributeDefinition(name = "Apply sub-pixel rendering", description = "If enabled characters will be rendered using sub-pixel precision, which provides more accurate results when scaling",
                options = {
                        @Option(label = "Enabled", value = "true"),
                        @Option(label = "Disabled", value = "false")
                }
        )
        boolean subPixelRendering() default DEFAULT_SUBPIXEL_RENDERING;

        @AttributeDefinition(name = "Optimise rendering for", description = "Should rendering optimise for speed or quality?",
                options = {
                        @Option(label = "Quality", value = "true"),
                        @Option(label = "Speed", value = "false")
                }
        )
        boolean renderHighQuality() default DEFAULT_RENDER_HIGH_QUALTIY;

        @AttributeDefinition(name = "Interpolation strategy", description = "Controls how image pixels are filtered or resampled during an image rendering operation. " +
                "Bicubic tends to provide the best quality, followed closely by bilinear, with nearest-neighbour giving the lowest quality results at a trade-off with computational complexity. ",
                options = {
                        @Option(label = "Bicubic", value = "BICUBIC"),
                        @Option(label = "Bilinear", value = "BILINEAR"),
                        @Option(label = "Nearest-neighbour", value = "NEAREST_NEIGHBOUR")
                }
        )
        Interpolation renderInterpolation() default Interpolation.BILINEAR;

        enum Interpolation {
            BILINEAR(RenderingHints.VALUE_INTERPOLATION_BILINEAR),
            BICUBIC(RenderingHints.VALUE_INTERPOLATION_BICUBIC),
            NEAREST_NEIGHBOUR(RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);

            private final Object value;

            Interpolation(Object value) {
                this.value = value;
            }

            public Object getValue() {
                return value;
            }
        }
    }

    /**
     * Component activation handler
     *
     * @param config current configuration state
     */
    @Activate
    private void activate(Config config) {
        applyConfig(config);
    }

    /**
     * Component modification handler
     *
     * @param config current configuration state
     */
    @Modified
    private void modified(Config config) {
        applyConfig(config);
    }

    /**
     * Applies new configuration values
     *
     * @param config the new configuration
     */
    private void applyConfig(Config config) {
        supportedMimeTypes = config.supportedMimeTypes();

        antiAliasType = config.antiAlias() ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF;
        subPixelRendering = config.subPixelRendering() ? RenderingHints.VALUE_FRACTIONALMETRICS_ON : RenderingHints.VALUE_FRACTIONALMETRICS_OFF;
        renderQuality = config.renderHighQuality() ? RenderingHints.VALUE_RENDER_QUALITY : RenderingHints.VALUE_RENDER_SPEED;
        renderInterpolation = config.renderInterpolation().getValue();

        LOG.info("Configured with MIME types", (Object[]) supportedMimeTypes);

    }

    /**
     * Extracts the MIME type from the original asset
     *
     * @param asset the Asset to extract metadata from
     * @return the extracted metadata
     */
    @Override
    public ExtractedMetadata extractMetadata(Asset asset) {
        ExtractedMetadata metadata = new ExtractedMetadata();
        this.setMimetype(metadata, asset);
        return metadata;
    }

    /**
     * Attempts to create an image representation of the first slide in the document at the default size.
     *
     * @see #getImage(Rendition, Dimension)
     */
    @Override
    public BufferedImage getImage(Rendition rendition) throws IOException {
        // Use the default dimensions
        return this.getImage(rendition, null);
    }

    /**
     * Attempts to create an image representation of the first slide in the document
     *
     * @param rendition the rendition to create the image from
     * @param dimension the size of the requested image
     * @return an image of the first slide of the document, or null if an image could not be generated
     * @throws IOException if the document could not be read, or the image written
     */
    @Override
    public BufferedImage getImage(Rendition rendition, Dimension dimension) throws IOException {

        Asset asset = rendition.getAsset();

        try (InputStream inputStream = asset.getOriginal().getStream()) {
            try (XMLSlideShow slideShow = new XMLSlideShow(inputStream)) {
                if (!slideShow.getSlides().isEmpty()) {
                    LOG.info("Generating image for {}", asset.getPath());
                    return slideShowToImage(slideShow);
                }
            }
        }

        return null;
    }

    /**
     * @return the MIME types supported by this handler
     */
    @Override
    public String[] getMimeTypes() {
        return supportedMimeTypes;
    }

    /**
     * Converts the first slide of a slideshow to an image
     *
     * @param slideShow  the slideshow to be converted
     * @return a byte array representation of the first slide's PNG thumbnail
     * @throws IOException if the PNG fails to be written to the output stream
     */
    private BufferedImage slideShowToImage(XMLSlideShow slideShow) {

        Stopwatch timer = Stopwatch.createUnstarted();
        if (LOG.isDebugEnabled()) {
            timer.start();
        }

        // The POI library needs the correct classloader otherwise it will not have visibility of the necessary classes.
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(this.getClass().getClassLoader());

        try {
            // Get the first slide
            XSLFSlide slide = slideShow.getSlides().get(0);

            // Create a canvas the size of the thumbnail
            Dimension pageSize = slideShow.getPageSize();
            LOG.debug("Page size {} x {}", pageSize.width, pageSize.height);

            BufferedImage img = new BufferedImage(pageSize.width, pageSize.height, BufferedImage.TYPE_INT_RGB);

            // Initialise the graphics engine
            Graphics2D graphics = img.createGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, antiAliasType);
            graphics.setRenderingHint(RenderingHints.KEY_RENDERING, renderQuality);
            graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, renderInterpolation);
            graphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, subPixelRendering);

            // Start with a white background
            graphics.setPaint(Color.white);
            graphics.fill(new Rectangle2D.Float(0, 0, pageSize.width, pageSize.height));

            // Draw the first slide onto the canvas
            slide.draw(graphics);

            return img;

        } finally {
            // Reset the classloader to it's original state
            Thread.currentThread().setContextClassLoader(classLoader);

            if (LOG.isDebugEnabled()) {
                LOG.debug("Generated image from slideshow in {}ms", timer.stop().elapsed(TimeUnit.MILLISECONDS));
            }
        }

    }


}

